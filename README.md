# Delta-nerworks host

An [Ansible][1] role for customizing [Delta-networks][2] hosts.


[1]: https://docs.ansible.com/
[2]: https://www.delta-networks.de/